# Overview
- Continuation of previous conference paper "Inherent Damping in a Bypass River" that is accepted and will be presented at IAHR world congress in Panama September 2019.

- This conference paper could either be submitted to [River Flow](https://riverflow2020.org/) to be held in Delft in July 2020 or to [13th International Symposium on Ecohydraulics](https://ecoenet.files.wordpress.com/2019/03/brochure_first_call_v3.pdf) to be held in Lyon in May 2020.

- Abstract submitted to River Flow in Delft in July 2020.

- This paper should investigate parameters that affects the transient behavior of the water level, some parameters are
  * Gate closing time
  * Roughness of the river bed
  * Downstream coordinate
  * Initial water level
- This paper is the foundation of Paper 1 which will investigate more of the ecohydraulics in the bypass reach

## Data Assimilation

- Use OpenDA for calibration of roughness and boundary conditions
- Same validation data as for the other conference paper (unfortunately pretty weak for the scope of this paper)
- Use validation data for calibration
- Design and "cut" a 50-21-50 m³/s case and use this case for data assimilation

- It can be of interest to investigate when the discharge and water level is increasing, I imagine that the water level increase can be a function of the roughness. However, it's not as important for ecohydraulics as long as there are no "wash away" effects.

- Investigate the small lake as it's being filled. In the case where the river is dry I noticed that it can take several minutes for the small lake to fill up before the flow continues downstream. It can be interesting to investigate and maybe exploit this.
