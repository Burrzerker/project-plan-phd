# Environmental Flows
## Background
There is a well established relation between the discharge in a river system and the  health of a fluvial (running water) ecosystem and it's accompanying floodplains and wetlands (where applicable). The field of Environmental Flow Assesment (EFA) aims to quantify the flows (and flow parameters) necessary for either the continued health of a river system where exploitation is planned, or as a tool to improve the ecological conditions in an already exploited river system. [1]

The origins of the first EFA models can be traced back to the USA in the 1940's. These models were more or less *ad hoc* but more formalized in the 1970's. In literature EFA models first started to appear in the 1980's. Initially EFA models were mostly designed to maintenance of locally important freshwater fisheries, mainly salmonoid species were concerned. This is also the case for many countries today such as Norway (*my note*). The aim was mainly to find a minimum flow that would fulfill parameters required for a relevant habitat for a specific species. It was assumed that conditions suitable for a specific species was also suitable for the entire ecosystem. [1]

Today it is widely accepted that more holistic (entire ecosystem) approaches for environmental flows are more suitable.

## Methods
Usually there are four different kinds of methods that are being used in EFA. Hydrological methods, habitat simulation methods, hydraulic rating methods and holistic methods.

### Hydrological Methods

Hydrological methods are based on available hydrological data of the relevant river stretch or river system. Data such as historical monthly or daily average flows are used to derive the normal conditions of the river. These conditions are the recommended environmental flows conditions. If there is no exploitation of the river this would represent the entirely "natural" conditions.

The environmental flow requirement (EFR) is often given by the proportion of the flow that is necessary to maintain the health of the river, usually this is represented by the 95th percentile, i.e. the flow that is equal or exceeded 95% of the time. Sometimes additional parameters like catchment variables, hydraulic, biological or morphological parameters are also used. [1]

Hydrological models are mainly used in the planning stage of exploitation since they are easy to use with only historical data.

### Hydraulic Rating Methods

Hydraulic rating methods use the changes in certain hydraulic variables such as hydraulic radius or wetted perimeter as a function of discharge as a predictor of habitat factors. Often breaking points are identified where a significant percentage decrease in habitat quality occurs with a reduction in discharge. Assuming that this breaking point corresponds to the lowest flow that is acceptable, it can be used as an EFR. [1] For instance, it is known that salmon and trout require very specific water depths and velocities for spawning. Thus the wetted perimeter corresponding to this depth could be used as a variable. [2]

### Habitat Simulation Methods


## Sources

 1. Environmental flow assessment with emphasis on holistic methodologies, 2004, *Arthington et al*
 2. Conversation with Anders
