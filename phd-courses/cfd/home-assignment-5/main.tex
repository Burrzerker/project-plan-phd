\documentclass[11pt]{scrartcl}

\usepackage{bm}
\usepackage{enumitem}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{float}
\usepackage{listings}
\usepackage{siunitx}
\usepackage{pythonhighlight}
\usepackage[top=2.5cm, bottom=2.5cm, left=2.5cm, right=2.5cm]{geometry}

\newcommand{\R}{\mathbb{R}}
\title{\textbf{Home Assignment 5 - CFD PHD Course}}
\author{Anton Burman}
\date{}
\begin{document}

\maketitle

\section{Problem Statement}
The purpose of this assignment is to study the differences between two different mesh approaches commonly used in CFD, colocated and staggered grids. To do this the performance of each approach is studied by solving the classical \textit{lid driven cavity} problem where there are three steady walls and one moving lid. The governing equations as for all fluid dynamics is the Navier-Stokes equations
\begin{equation}
\frac{\partial}{\partial t}\int_{V} \rho u_i~dV + \int_S \rho u_i \mathbf{v} \cdot \mathbf{n}~dS = \int_S \mathbf{t}_i \cdot \mathbf{n}~dS + \int_{V} \rho b_i~dV
\end{equation}
\begin{equation}
\frac{\partial}{\partial t} \int_V \rho~dV + \int_S \rho \mathbf{v} \cdot \mathbf{n}~dS = 0
\end{equation}
Where $\rho = 1~kg/m^3$ and $\mu=\num{1e-3}~Pa\cdot s$. In this report a solver written in Fortran77 that is available at \textit{http://www.cfd-peric.de/} will be used. This solver uses the SIMPLE method for pressure correction. The comparison between the two configurations will be done by performing a Richardson extrapolation based on the total shear stress on the boundary for the different meshes. In this report all meshes are uniform.
\section{Theory}
\subsection{Wall Shear Stress}
The wall shear stress for a flat plate arises from the tangential movement in proximity of the boundary. For our case there are thus two equations for the wall shear stress. The shear stress for the north and south boundaries are
\begin{equation}
	\tau_{n, s} = \mu \frac{\partial u}{\partial y}
\end{equation}
and the west and east boundaries
\begin{equation}
\tau_{w, e} = \mu \frac{\partial v}{\partial x}
\end{equation}
where $u$ is the horizontal velocity component, $v$ is the vertical velocity component, $y$ is the vertical coordinate, $x$ is the horizontal coordinate and $\mu$ is the dynamic viscosity. The total shear stress on all boundaries is then the sum of all local shear stress in each grid point next to the boundary
\begin{equation}
	\tau_{j} = \sum_{i=1}^N \tau_i ,~j=e,w,n,s
\end{equation}
where $N$ is the number of nodes along the boundary. Then the total shear stress is
\begin{equation}
	\tau_{tot} = \tau_{s} + \tau_{n} + \tau_{e} + \tau_{w}.
\end{equation}

\subsection{Richardson Extrapolation}
Richardson extrapolation is used to find a mesh independent solution for some variable of relevance by varying the mesh size. The method implemented in this report is described in \cite{ferziger2019} and \cite{richardson}. The first step is define a representative grid size for three different meshes, for the two-dimensional case one suggestion is 
\begin{equation}
	h = \left[\frac{1}{N}\sum_{i=1}^N(\Delta A_i)\right]^{1/2}
\end{equation}
which reduces to 
\begin{equation}
	h = \frac{1}{N}
\end{equation}
if the grid is uniform. Step two is to perform simulations on three different grids and extract a representative variable of choice $\phi$. In this step we also define the variables $r_{32} = h_3/h_2$, $r_{21}=h_2/h_1$, $\varepsilon_{32} = \phi_3 - \phi_2$ and $\varepsilon_{21} = \phi_2 - \phi_1$. Now the apparent order of the solution can be computed with the implicit equation
\begin{equation}
	\frac{1}{r_{21}}\left|ln\left|\frac{\varepsilon_{32}}{\varepsilon_{21}}\right|+ ln\left(\frac{r_{21}^p - s}{r_{32}^p - s}\right)\right| - p = 0
\end{equation}
where $p$ is the apparent order and 
\begin{equation}
	s = sign\left(\frac{\varepsilon_{32}}{\varepsilon_{21}}\right).
\end{equation}
Then the extrapolated value is
\begin{equation}
	\phi_{ext} = \frac{r_{21}^p\phi_1 - \phi_2}{r_{21}^p-1}
\end{equation}
\section{Results}
The residuals for each mesh and the $u$ and $v$ vector fields for each mesh was extracted from the Fortran77 solver. The three different meshes that were used had $h$ values $1/40$, $1/60$ and $1/80$. 
\subsection{Residuals}
The residuals for the staggered and colocated cases can be seen in figure 1 and 2. In both plots we can see that it requires more iterations for convergence the finer the mesh is. This is because information propagates faster in a coarser mesh compared to a fine mesh. Hence the solution should be achieved faster for a coarse mesh as long as the solver doesn't crash. We can also see that the slope of the residuals is steeper for the coarser mesh.
\begin{figure}[H]
	\includegraphics[width=\linewidth]{colocated_residuals.png}
	\caption{Colocated residuals for the three different meshes.}
\end{figure}
\begin{figure}[H]
	\includegraphics[width=\linewidth]{staggered_residuals.png}
\end{figure}
\subsection{Streamlines}
In figure 3 the streamlines for each respective case has been plotted. There are no dramatic difference between the mesh sizes or the grid configuration.
\begin{figure}[H]
	\includegraphics[width=\linewidth]{streamlines.png}
\end{figure}
\subsection{Shear Stress}
The shear stress for the six different cases has been tabulated in table 1.
\begin{table}[H]
\centering
\caption{Total shear stress $\tau$ in $Pa$ for the six cases.}
\begin{tabular}{l c c c }
	$N$& 40 & 60 &80 \\
	\hline \hline 
	Colocated       & -0.548  & -1.056 & -1.713\\
	Huen's Method   & -0.576  & -1.081  & -1.732
\end{tabular}
\end{table}
\subsection{Richardson Extrapolation}
By solving equation 9 for both the staggered and the colocated configurations the apparent order of the solution was obtained. For the colocated configuration $p=-1.827$ was obtained and for the staggered configuration $p=-1.850$ was obtained. The extrapolated value was $\tau_{ext}=-1.827$ for the colocated configuration and $\tau_{ext}=-1.755$ for the staggered configuration.
\section{Python Code}
\inputpython{/home/anton/phd-related-code/cfd_course/assignment_5/assignment_5.py}{1}{200}
\begin{thebibliography}{9}
	
	\bibitem{ferziger2019}
	Joel H Ferziger,
	Milovan Perić,
	Robert L. Street
	\textit{Computational Methods for Fluid Dynamics},
	Springer Nature, Switzerland,
	4th edition,
	2020.
	\bibitem{richardson}
	"Procedure for Estimation and Reporting of Uncertainty Due to Discretization in CFD Applications." ASME. J. Fluids Eng. July 2008; 130(7): 078001. https://doi.org/10.1115/1.2960953

\end{thebibliography}
\end{document}
