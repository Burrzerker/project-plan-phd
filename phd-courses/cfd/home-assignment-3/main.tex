\documentclass[11pt]{scrartcl}

\usepackage{bm}
\usepackage{enumitem}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{float}
\usepackage{listings}
\usepackage{pythonhighlight}
\usepackage[top=2.5cm, bottom=2.5cm, left=2.5cm, right=2.5cm]{geometry}

\newcommand{\R}{\mathbb{R}}
\title{\textbf{Home Assignment 3 - CFD PHD Course}}
\author{Anton Burman}
\date{}
\begin{document}

\maketitle

\section{Problem Statement}
From home assignment 2 it is known that the governing equation for two-dimensional convection and diffusion is 
\begin{equation}
\frac{\partial \left(\rho u \phi\right)}{\partial x}  + \frac{\partial \left(\rho v \phi\right)}{\partial y} = \Gamma \left(\frac{\partial^2 \phi}{\partial x^2} + \frac{\partial^2 \phi}{\partial y^2}\right)
\end{equation}
where 
\begin{equation}
u = 1, ~ v = 0.5, ~ \Gamma = 0.025, ~ \rho = 1.
\end{equation}
We are considering a quadratic area of size 1x1 with boundary conditions
\begin{equation}
\phi(x,0) = 0,~ \phi(0,y) = 100, ~\phi(x,1) = 0, ~\phi(1,y) = 0.
\end{equation}
The purpose of this assignment is for the the UDS and CDS finite volume methods investigate the following statements:
\begin{itemize}
	\item What happens when the mesh size is varied
	\item What iterative methods are recommended for specific matrix properties?
	\item How does the choice of iterative method affect the solution?
	\item Can the preconditioned conjugate gradient method be used with UDS or CDS in this problem statement?
\end{itemize}
\section{Iterative Methods}
The UDS and CDS results in a linear system of equations with the shape
\begin{equation}
	A \bm{\phi} = Q
\end{equation}
where $A$ is dependent on which scheme you use. In general CFD problems the system of equations that arise are not linear, rather they are put through some process where they are first linearized and then solved iteratively. In this report we are applying iterative methods to problems that are linear in nature. Although the equations could be solved exactly there could be significant decrease in computational time for the system when using an iterative method. 
\subsection{Matrix Properties of UDS and CDS}
A matrix is said to be symmetric if it's equal to it's own transpose
\begin{equation}
	A = A^T.
\end{equation}
Furthermore, a matrix is positive definite if it is symmetric and
\begin{equation}
	\bm{x}^T A \bm{x} > 0,~\text{for all} ~x \in \R^n.
\end{equation}
A matrix is diagonally dominant if 
\begin{equation}
	|A_{ii}| \geq \sum_{j\neq i} |A_{ij}|,~\text{for all}~i.
\end{equation}
These properties are important to make an informed decision regarding which iterative method should be used. With the given boundary conditions the properties described above have been tabulated in Table 1 for the UDS and CDS respectively.
\begin{table}[H]
	\centering
	\caption{Matrix properties for UDS and CDS.}
	\begin{tabular}{l | ll}
		& UDS & CDS \\
		\hline
		Symmetric           & No  & No  \\
		Positive Definite   & No  & No  \\
		Diagonally Dominant & No  & No 
	\end{tabular}
\end{table}
\subsection{Conjugate Gradient Method}
The conjugate gradient method converts equation 4 to an optimization problem where we try to find the minimum of
\begin{equation}
	F = \frac{1}{2} \bm{\phi}^T A \bm{\phi} - \bm{\phi}^T Q.
\end{equation}
The conjugate gradient method is suited for matrices that are symmetric and positive definite \cite{ferziger2019}. By studying Table 1 we can see that the conjugate gradient method is not suited for our matrices.

\subsection{Chosen Iterative Methods}
One way to use the conjugate gradient method for asymmetric matrices is by modifying the solution matrix $A$ so that
\begin{equation}
	\begin{pmatrix}
		0 & A \\
		A^T & 0
	\end{pmatrix}
	\begin{bmatrix}
	\bm{\psi} \\
	\bm{\phi}
	\end{bmatrix} =
	\begin{bmatrix}
	\bm{Q} \\
	0
	\end{bmatrix}. 
\end{equation}
This guarantees that the solution matrix is always symmetric. A more stable version of the biconjugate gradient method known as BICG Stable is also available. The last iterative method to be investigated is the generalized minimum residual method (GMRES). GMRES is also a descent method and is popular because it is rather stable. All these methods are available through the python package SciPy.

\section{Results}
\subsection{Mesh Dependence}
\begin{figure}[H]
	\centering
	\includegraphics[scale=0.8]{comparison_8x8.png}
	\caption{Comparison between algorithms for a 8x8 mesh.}
\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[scale=0.8]{comparison_16x16.png}
	\caption{Comparison between algorithms for a 16x16 mesh.}
\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[scale=0.8]{comparison_32x32.png}
	\caption{Comparison between algorithms for a 32x32 mesh.}
\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[scale=0.8]{comparison_64x64.png}
	\caption{Comparison between algorithms for a 64x64 mesh.}
\end{figure}
The biconjugate gradient method performs well for the smaller grid sizes but we notice that there is divergence for larger meshes both for UDS and CDS. The BICG Stable method is stable for all mesh sizes and converges also for the largest mesh size in both cases. The GMRES is the method that appears to be most grid independent and it also converges for all meshes.
\subsection{Execution Time}
\begin{table}[H]
	%\centering
	
	\caption{Execution time for different meshes and algorithms. Time in seconds.}
	\hspace{-30pt}\begin{tabular}{l | cccccc}
		& UDS BICG & CDS BICG & UDS BICG Stable & CDS BICG Stable & UDS GMRES & CDS GMRES \\
		\hline
		8x8     & 0.011 & 0.021 & 0.003 & 0.003 & 0.001 & 0.009 \\
		16x16   & 0.104 & 0.028 & 0.023 & 0.053 & 0.042 & 0.036 \\
		32x32   & 0.535 & 0.660 & 0.190 & 0.377 & 0.530 & 0.966 \\
		64x64   & 4.37 & 7.62 & 2.34 & 5.27 & 5.68 & 6.24
	\end{tabular}
\end{table}
From table 2 we notice a general trend, the biconjugate gradient method is the slowest method, the BICG stable method is fastest and the GMRES is inbetween for all cases. It is possible that the GMRES would be faster than BICG stable if proper preconditioning was performed before execution.
\section{Python Code}
\inputpython{/home/anton/phd-related-code/cfd_course/assignment_3/assignment_3.py}{1}{200}
\begin{thebibliography}{9}
	
	\bibitem{ferziger2019}
	Joel H Ferziger,
	Milovan Perić,
	Robert L. Street
	\textit{Computational Methods for Fluid Dynamics},
	Springer Nature, Switzerland,
	4th edition,
	2020.
	
	
\end{thebibliography}
\end{document}
