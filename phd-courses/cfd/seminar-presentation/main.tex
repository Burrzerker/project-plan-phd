\documentclass[pdf, 10pt, aspectratio=169]{beamer}
\usetheme{Copenhagen}

\mode<presentation> {


\usetheme{Malmoe}

}

\usepackage{graphicx} % Allows including images
\usepackage{booktabs} % Allows the use of \toprule, \midrule and \bottomrule in tables
\usepackage{multimedia}
\usepackage[makeroom]{cancel}
%\usepackage[backend=bibtex]{biblatex}
\usepackage{media9}
\usefonttheme[onlymath]{serif}

\title[]{Seminar 5 - Solution of the Navier-Stokes Equations: Part 1}

\author{Anton Burman \& Tobias Forslund} % Your name
\institute[LTU]
{
\medskip
\includegraphics[width=0.3\linewidth]{figures/logoB.png}
}
\date{}

\begin{document}

\begin{frame}
\titlepage % Print the title page as the first slide
\end{frame}

\begin{frame}
	\frametitle{Fundamentals}
	All fluid flow is governed by the Navier-Stokes Equations. The equations consists of three momentum equations and one continuity equation. \pause The integral form is
	\begin{equation}
		\frac{\partial}{\partial t}\int_{V} \rho u_i~dV + \int_S \rho u_i \mathbf{v} \cdot \mathbf{n}~dS = \int_S \mathbf{t}_i \cdot \mathbf{n}~dS + \int_{V} \rho b_i~dV
	\end{equation}
	\begin{equation}
		\frac{\partial}{\partial t} \int_V \rho~dV + \int_S \rho \mathbf{v} \cdot \mathbf{n}~dS = 0
	\end{equation}
	where $\mathbf{t}_i = \tau_{ij}\mathbf{i}_j-p\mathbf{i}_j$ and $\tau_{ij}$ is the viscous part of the stress tensor. \pause The Navier-Stokes equations can be analogously written with tensor notation
	\begin{equation}
		\frac{\partial(\rho u_i)}{\partial t}+\frac{\partial(\rho u_j u_i)}{\partial x_j}= \frac{\partial \tau_{ij}}{\partial x_j} - \frac{\partial p}{\partial x_i} + \rho b_i
	\end{equation}
	\begin{equation}
		\frac{\partial \rho}{\partial t} + \nabla\cdot (\rho \mathbf{v}) = 0
	\end{equation}
\end{frame}
\begin{frame}
	\frametitle{Convection and viscous terms}
	The convective terms in the Navier-Stokes equations
	\begin{equation*}
		\frac{\partial(\rho u_j u_i)}{\partial x_j} ~~~\text{or} ~~~ \int_S \rho u_i \mathbf{v} \cdot \mathbf{n}~dS
	\end{equation*}
	can be discretized using the methods we discussed in chapter 3 and 4. \pause 
	The viscous terms 
	\begin{equation*}
		\frac{\partial \tau_{ij}}{\partial x_j}  ~~~\text{or} ~~~ \int_S (\tau_{ij}\mathbf{i}_j)\cdot \mathbf{n}
	\end{equation*}
	are the analog to the diffusive terms in the convection-diffusion equation, the obvious difference being that the Navier-Stokes equations contain vector variables.
\end{frame}
\begin{frame}
	\frametitle{Convection and viscous terms continued}
	If we can assume incompressible flow and a Newtonian fluid then the viscous stress term is
	\begin{equation}
		\tau_{ij} = \mu \left(\frac{\partial u_i}{\partial x_j}+\frac{\partial u_j}{\partial x_i}\right).
	\end{equation} \pause
	The term 
	\begin{equation*}
		\frac{\partial }{\partial x_j}\left(\mu \left(\frac{\partial u_i}{\partial x_j}\right)\right)
	\end{equation*}
	is analog to the diffusive term in the convection-diffusion equation and can be discretized with the methods in chapter 3 and 4.\pause The other term 
	\begin{equation*}
		\frac{\partial }{\partial x_j}\left(\mu \left(\frac{\partial u_j}{\partial x_i}\right)\right)
	\end{equation*}
	is 0 for the cases where the viscosity is constant. In the cases where this is not true this term is often treated explicitly in a similar way as the diffusive term.
\end{frame}
\begin{frame}
	\frametitle{Pressure term}
	The gradient of the pressure appears in the differential form of the Navier-Stokes equations
	\begin{equation*}
		\frac{\partial p}{\partial x_i}.
	\end{equation*}
	This can be approximated with for instance finite differences.  \pause
	In the integral formulation the pressure term is
	\begin{equation*}
		\int_S p \mathbf{i}_i \cdot \mathbf{n}~dS
	\end{equation*}
	and can be discretized with the FV methods described in chapter 4.
\end{frame}
\begin{frame}
	\frametitle{Conservation properties: mass}
	A property of the Navier-Stokes equations is that the change of momentum in any control volume is due to \pause
	\begin{itemize}
		\item flow through the surface of the control volume \pause
		\item forces acting on the control volume surface (i.e. pressure gradient) \pause
		\item volumetric body forces (i.e. gravity)
	\end{itemize}

\end{frame}
\begin{frame}
	\frametitle{Conservation properties: energy}
	For incompressible isothermal flows the only energy of significance is the kinetic energy. An equation for the kinetic energy can be constructed by taking the scalar product of the momentum equation with the velocity, by further using the Gauss theorem the equation is
	\begin{equation}
	\frac{\partial}{\partial t} \int_V \rho \frac{v^2}{2}~dV = - \int_S \rho \frac{v^2}{2}\mathbf{v}\cdot \mathbf{n}~dS - \int_S p \mathbf{v} \cdot \mathbf{n}~dS + \int_S (S\cdot\mathbf{v})\cdot \mathbf{n}~dS - \int_V (S:\nabla\mathbf{v}-p\nabla\mathbf{v}+\rho \mathbf{b}\cdot \mathbf{v})~dV
	\end{equation}
	where $:$ is the tensor double dot product operator, $S$ is the viscous part of the stress tensor and $b$ is a body force.
\end{frame}
\begin{frame}
	\frametitle{Conservation properties: energy continued}
	\begin{itemize}
		\item The kinetic energy within the control volume is not changed by the action of convection or pressure within the control volume. \pause
		\item The kinetic energy equation is a consequence of the momentum equations rather than a conservation equation.\pause
		\item Ensuring global energy conservation is often used to provide numerical stability. If no energy is created within the CV then the velocity will remain bounded. \pause
		\item If energy conservation is to be obtained it puts constraints on which spatial discretizations that can be used for the different terms in the momentum equation (pages 188-191 in \cite{ferziger2019}).\pause
		\item In addition to the spatial discretization, the time differencing scheme can destroy the energy conservation and should be chosen with care. \pause
	\end{itemize}
\end{frame}
\begin{frame}
	\frametitle{The problem with pressure and the pressure equation}
	\begin{itemize}
		\item The Navier-Stokes equation doesn't have an independent equation for the pressure, still the pressure gradient affects all the velocity components in the momentum equations. \pause
		\item For compressible flows the continuity equation can be used to couple the velocity and the pressure, this is however not suitable for incompressible flows. \pause
		\item By taking the divergence of the momentum equation it is possible to construct the following Poissson equation for the pressure
		\begin{equation}
			\nabla\cdot(\nabla p) = -\nabla \left(\nabla \cdot (\rho \mathbf{v}\mathbf{v} - S)-\rho \mathbf{b}+\frac{\partial(p\mathbf{v})}{\partial t}\right)
		\end{equation}\pause
		in cartesian tensor notation it is written as
		\begin{equation}
			\frac{\partial}{\partial x_i}\left(\frac{\partial p}{\partial x_i}\right) = -\frac{\partial}{\partial x_i}\left(\frac{\partial}{\partial x_j}(\rho u_iu_j-\tau_ij)\right) + \frac{\partial(\rho b_i)}{\partial x_i} + \frac{\partial^2 \rho}{\partial t^2}.
		\end{equation}\pause
		This equation simplifies further when the density, viscosity and body forces are constant. The viscous and unsteady contributions vanish due to the continuity equation
		\begin{equation}
			\frac{\partial}{\partial x_i}\left(\frac{\partial p}{\partial x_i}\right) = -\frac{\partial}{\partial x_i}\left(\frac{\partial \rho u_iu_j}{\partial x_j}\right)
		\end{equation}
	\end{itemize}
\end{frame}
\begin{frame}
	\frametitle{The role of pressure in the Navier-Stokes equations}
	\begin{itemize}
		\item 	Let us consider a case where we have a velocity field $\mathbf{v}^*$ that doesn't satisfy continuity. From this we want to create a new velocity field $\mathbf{v}$ that satisfies continuity and is close to $\mathbf{v}^*$. \pause 
		This is analog to minimizing
		\begin{equation}
		\tilde{R} = \frac{1}{2}\int_V (\mathbf{v}(\mathbf{r}) - \mathbf{v}^*(\mathbf{r}))^2~dV
		\label{r-tilde}
		\end{equation}
		with continuity constraint
		\begin{equation}
		\nabla \cdot \mathbf{v}(\mathbf{r}) = 0
		\end{equation} \pause
		\item 	Problems like this can be solved using \textit{calculus of variations}  \cite{logan2013applied}.\pause
		\item Equation 10 is an example of a \textit{functional}.
	\end{itemize}



\end{frame}
\begin{frame}
	\frametitle{The role of pressure in the Navier-Stokes equations continued}
	\begin{itemize}
		\item One method commonly used is the Lagrange multiplier method, rewriting equation~\ref{r-tilde} with a Lagrange multiplier $\lambda(\mathbf{r})$ yields
		\begin{equation}
			R = \frac{1}{2} \int_V (\mathbf{v}(\mathbf{r}) - \mathbf{v}^*(\mathbf{r}))^2~dV - \int_V \lambda(\mathbf{r}) \nabla\cdot \mathbf{v}(\mathbf{r})~dV
		\end{equation}
		if we suppose that 
		\begin{equation}
			R_{min} = \frac{1}{2}\int_V(\mathbf{v}^+(\mathbf{r})-\mathbf{v}^*(\mathbf{r}))^2~dV
		\end{equation}
		where $\mathbf{v}^+(\mathbf{r})$ is the minima, we can derive a relation on the form
		\begin{equation}
			\mathbf{v}^+ - \mathbf{v}^*(\mathbf{r}) +\nabla\lambda(\mathbf{r}) =0
			\label{eq:v}
		\end{equation}
	\end{itemize}	
\end{frame}
\begin{frame}
	\frametitle{The role of pressure in the Navier-Stokes equations continued}
	\begin{itemize}
		\item We recall that $\mathbf{v}^+(\mathbf{r})$ satisfies the continuity equation. This together with taking the divergence of equation \ref{eq:v} yields
		\begin{equation}
			\nabla^2 \lambda(\mathbf{r})=\nabla \cdot \mathbf{v}^*(\mathbf{r})
		\end{equation}
		which is a Poisson equation. The Lagrange multiplier takes the role of pressure. \pause
		\item The momentum equations determine the velocity components in the Navier-Stokes equations. \pause
		\item The continuity equation doesn't contain the pressure but above we have argued why the role of the pressure is to satisfy continuity.
	\end{itemize}
\end{frame}

\begin{thebibliography}{9}
	
	\bibitem{ferziger2019}
	Joel H Ferziger,
	Milovan Perić,
	Robert L. Street
	\textit{Computational Methods for Fluid Dynamics},
	Springer Nature, Switzerland,
	4th edition,
	2020.
	
	\bibitem{logan2013applied}
		J David Logan,
		\textit{Applied mathematics},
		Third edition,
		2013,
		John Wiley \& Sons
	
		
	
\end{thebibliography}

\end{document}