\documentclass[pdf, 10pt, aspectratio=169]{beamer}
\usetheme{Copenhagen}
\mode<presentation> {
	\usetheme{Malmoe}	
}

\usepackage{graphicx}
\usepackage{tikz}
\usefonttheme[onlymath]{serif}

\title[]{Anton Burman - 1 Year+ Presentation}

\author{Anton Burman}
\institute{Lule\aa~University of Technology}
\date{January 17, 2020}
\institute[LTU] 
{
	\inst{}
	\textbf{Supervisors}\\
	Dr. Gunnar Hellstr\"om \\
	Dr. Anders Andersson
}
\begin{document}
\begin{frame}
	\titlepage
	
\end{frame}


\begin{frame}
	\frametitle{Summary}
	\begin{itemize}
		\item Background
		\begin{itemize}
			\item HydroFlex
			\item Work Package 5
			\item Fundamentals of river modelling
		\end{itemize}
		\item Research
		\begin{itemize}
			\item Validate Flow Model of Ume \"Alv
			\item Evaluation of Passive Methods to Mitigate Discharge Fluctuations in Ume \"Alv
		\end{itemize}
		\item Conclusions so Far and Future Work
		\item Miscellaneous
		\begin{itemize}
			\item Courses
			\item Work outside the scope of my PhD	
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}
	\begin{columns} 
		\frametitle{Background - HydroFlex}
		\begin{column}{.5\textwidth} 
			\begin{block}{HydroFlex}
				\begin{itemize}
					\item HydroFlex is a EU funded research consortium spanning many countries in the EU.
					\item HydroFlex (Increasing the value of Hydropower through increased Flexibility).
					\item "HydroFlex addresses ... focuses on the need to develop new technologies, generators and turbine designs to increase the flexibility of hydropower plants while mitigating environmental impacts." 
				\end{itemize}
			\end{block}
		\end{column}
	
		\begin{column}{.5\textwidth}
			\centering
			\vspace{-0.5 cm}
			\begin{figure}
			\includegraphics[width=1\linewidth]{figures/hydroflex-website.png}
			\caption{HydroFlex website (HydroFlex 2020).}
			\label{fig:hydroflex-web}
			\end{figure}
		\end{column}
	\end{columns} 
\end{frame}

\begin{frame}

	\frametitle{HydroFlex WP:5}
		\begin{block}{HydroFlex}
			\begin{itemize} 
				\item Social Acceptance and Mitigation of Environmental Impact.\pause
				\item WP 5.1 - Technology for mitigation of highly fluctuating discharges into downstream river.\pause
				\item \textbf{WP 5.2 - Flow scenario modelling.}\pause
				\item WP 5.3 - Evaluating the efficiency of the mitigation technology on fish populations.\pause
				\item WP 5.4 - Assessing the public acceptance of increased ramping rates.\pause
				\end{itemize}
		\end{block}
		\begin{block}{WP 5.2 - Goals}
			\begin{itemize}
				\item Evaluation of passive methods to mitigate discharge fluctuations in Nidelva and Ume \"alv.\pause
				\item Validate flow models of Nidelva and Ume \"alv under present regime.\pause
				\item Model possible future scenarios (including 30 start and stops).\pause
				\item Model future scenarios with passive and active (WP 5.1) mitigations.
			\end{itemize}
		\end{block}

\end{frame}

\begin{frame}
\frametitle{Modelling the flow in a river}
	\begin{block}{The problem of Navier-Stokes equations in river flows}
		In principle the Navier-Stokes equations are the governing equations of river flows
		\begin{equation}
			\begin{cases}
				\nabla \cdot \mathbf{u} = 0 \\
				\frac{D\mathbf{u}}{Dt} = -\frac{1}{\rho} \nabla p + \mathbf{F} +\nu \nabla^2\mathbf{u}.
			\end{cases}
		\end{equation} \pause
		It's problematic to model entire river reaches with the full Navier-Stokes equations due to\pause
		\begin{itemize}
			\item Large length scales, the length of a typical reach is on the order tens of kilometers \pause
			\item Large time scales, the time scale is typically on the order of days or weeks, in extreme cases hundreds of years (morphology)\pause
		\end{itemize}
	\end{block}
	\begin{block}{Assumptions for Shallow-Water equations}
		\begin{itemize}
			\item Friction forces can be considered as a body force i.e. $\nu \nabla^2\mathbf{u}=0$.\pause
			\item Pressure is approximately hydrostatic $p=\rho g h$.\pause
			\item The horizontal length scale is much longer than the veritcal scale (length of reach $\gg$ depth)
		\end{itemize}
	\end{block}
\end{frame}

\begin{frame}
	\begin{columns} 
	\frametitle{Modelling the flow in a river: continued}
	\begin{column}{.5\textwidth} 
		\begin{block}{Shallow Water Equations}
			Given the assumptions in the previous slide and the free surface (exploiting the Leibniz rule of integration) and no-slip boundary conditions it's possible to derive the 2D \textit{Shallow Water Equations}
			\begin{equation*}
				\begin{cases}
					\frac{\partial u}{\partial t} + u\frac{\partial u}{\partial x} + v \frac{\partial u}{\partial y} + g \frac{\partial \eta}{\partial x} = 0 \\
					\frac{\partial v}{\partial t} + u\frac{\partial v}{\partial x} + v \frac{\partial v}{\partial y} + g \frac{\partial \eta}{\partial y} = 0 \\
					\frac{\partial \eta}{\partial t} + \frac{\partial}{\partial x}\left(u(\eta + h)\right) + \frac{\partial}{\partial y}(v(\eta + h)) = 0
				\end{cases}
			\end{equation*}
			where $u$ and $v$ are the respective velocity components, $\eta$ is the displacement of the water surface, $g$ is the gravitational force and $h$ is the water depth. This is one way of writing the shallow water equations, it can be derived to include more terms also.
		\end{block}
	\end{column}
	
	\begin{column}{.5\textwidth}
		\centering
		\begin{figure}
			\includegraphics[width=1\linewidth]{figures/shallow_coordinate.png}
			\caption{Arbitrary cartesian coordinate system for shallow water equations.}
			\label{fig:shallow}
		\end{figure}
	\end{column}
\end{columns} 
\end{frame}

\begin{frame}
	\frametitle{Modelling the flow in a river: continued}
	\begin{block}{Features of the Shallow Water equations}
		\begin{itemize}
			\item Non-linear inertia terms still exist.
			\item Transient terms still exist.
			\item Evolution of water surface and depth.
			\item Turbulence, an analogy to the 4/5ths law is derived in \cite{augier2019shallow}.
		\end{itemize}
	\end{block}
	\begin{block}{NOT features of the Shallow Water equations}
		\begin{itemize}
			\item Pressure effects outside hydrostatic pressure
			\item Viscous shear stresses
			\item Vertical velocities, these are not necessarily zero, just not resolved by the shallow water equations. These can be retrieved by integrating the continuity equation.
		\end{itemize}
	\end{block}
\end{frame}

\begin{frame}
	\frametitle{Research - Study Area Ume \"Alv}
	\begin{columns} 
		\begin{column}{.5\textwidth} 
			\begin{figure}
				\includegraphics[width=1\linewidth]{figures/stornorrfors.jpg}
				\caption{Stornorrfors bypass reach (my picture autumn 2019).}
				\label{fig:stornorrfors}
			\end{figure}
		\end{column}
		\begin{column}{.5\textwidth}
			\begin{figure}
				\includegraphics[width=1\linewidth]{figures/ume_river.png}
				\caption{Ume \"Alv around Ume\aa.}
				\label{fig:umeå}
			\end{figure}
		\end{column}
	\end{columns} 
\end{frame}

\begin{frame}
	\frametitle{Research - Validate Flow Model of Ume \"Alv}
	\begin{columns} 
		\begin{column}{.5\textwidth} 
			\begin{block}{IAHR World Congress 2019}
				\begin{itemize}
					\item Conference paper \textit{Inherent Damping in a Partially Dry River} presented at IAHR World Congress in Panama City, September 2019.
				\end{itemize}
			\end{block}
		\begin{figure}
			\includegraphics[width=1\linewidth]{figures/panama_city.jpg}
			\caption{Panama City skyline.}
			\label{fig:panama-city}
		\end{figure}
		\end{column}
		\begin{column}{.5\textwidth}
			\begin{figure}
				\includegraphics[width=1\linewidth]{figures/panama_presentation.jpg}
				\caption{Me presenting in Panama.}
				\label{fig:panama}
			\end{figure}
		\end{column}
	\end{columns} 
\end{frame}

\begin{frame}
\frametitle{Research - Validate Flow Model of Ume \"Alv}
	\begin{block}{Inherent Damping in a Partially Dry River}
		\begin{itemize}
			\item Objectives
			\begin{itemize}
				\item Calibrate Manning roughness in the reach
				\item Validate with available transient water level data
			\end{itemize}
		\end{itemize}
	\end{block}
	\vspace{-0.5 cm}
	\begin{columns} 
		\begin{column}{.33\textwidth} 
			\begin{figure}
				\includegraphics[scale=0.35]{figures/transient_response_1_4.png}
				\caption{Transient response in points 1-4.}
				\label{fig:1-4}
			\end{figure}
		\end{column}
		\begin{column}{.33\textwidth}
			\begin{figure}
				\includegraphics[scale=0.35]{figures/transient_response_5_8.png}
				\caption{Transient response in points 5-8.}
				\label{fig:5-8}
			\end{figure}
		\end{column}
		\begin{column}{.33\textwidth}
		\begin{figure}
			\includegraphics[scale=0.35]{figures/validation_points.png}
			\caption{Validation points.}
			\label{fig:validation-points}
		\end{figure}
	
		\end{column}
	\end{columns} 
\end{frame}

\begin{frame}
\frametitle{Research - Evaluation of Passive Methods to Mitigate Discharge Fluctuations in Ume \"Alv}
\begin{columns}
	\begin{column}{0.5\textwidth}
		\begin{block}{River Flow 2020}
			\begin{itemize}
				\item Conference paper \textit{Investigating damping properties in a bypass river} to be presented at the River Flow conference in Delft, Netherlands in July.
				\item Objectives
				\begin{itemize}
					\item Investigate how the upstream down closing time affects the transient behavior of the water level in the reach
					\item Investigate the inherent dispersion properties of the geometry in the reach
					\item Investigate the transient area change as a function of closing time
				\end{itemize}
			\end{itemize}
		\end{block}
	\end{column}
	\begin{column}{0.5\textwidth}
	\begin{figure}
		\includegraphics[width=1\linewidth]{figures/geometry.png}
		\caption{Calibrated Manning distribution (left), measured bathymetry (right).}
		\label{fig:geometry}
	\end{figure}
	\end{column}
\end{columns}
\end{frame}

\begin{frame}
	\frametitle{Research - Evaluation of Passive Methods to Mitigate Discharge Fluctuations in Ume \"Alv}

		\begin{figure}
			\includegraphics[width=1\linewidth]{figures/transient_response.png}
			\caption{Water level change along the centerline as a function of closing time.}
			\label{fig:response}
		\end{figure}
\end{frame}

\begin{frame}
	\frametitle{Research - Evaluation of Passive Methods to Mitigate Discharge Fluctuations in Ume \"Alv}
\begin{figure}
	\includegraphics[width=0.5\linewidth]{figures/transient_area.png}
	\caption{Transient area dynamics as a function of closing time.}
	\label{fig:transient-area}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{Research - Evaluation of Passive Methods to Mitigate Discharge Fluctuations in Ume \"Alv}
\begin{figure}
	\includegraphics[width=1\linewidth]{figures/difference_response.png}
	\caption{Difference between uniform Manning number and calibrated Manning distribution.}
	\label{fig:response-comparison}
\end{figure}
\end{frame}

\begin{frame}
	\frametitle{Conclusions so far and Future Work}
	\begin{block}{Conclusions} 
		\begin{itemize}
			\item Model was successfully calibrated with available time series data.\pause
			\item The Manning number doesn't affect the dynamics of the reach, only the actual water level.\pause
			\item The transient area has significantly different dynamics for faster closing times compared to slower closing times.\pause
		\end{itemize}
	\end{block}
	\begin{block}{Future work}
		\begin{itemize}
			\item Couple current results with ecohydraulics and publish in a journal.\pause
			\item Future scenarios modelling of downstream reach.\pause
			\item Future scenarios modelling with mitigations in the downstream reach.\pause
			\item Ecohydraulics in the downstream reach.\pause
			\begin{itemize}
				\item Habitat investigation for fish species.\pause
				\item Habitat investigation for birds (in collaboration with ornithologists at SLU).\pause
				\item Holistic approach to ecohydraulics in the reach.\pause
			\end{itemize}
		\end{itemize}
	\end{block}
\end{frame}

\begin{frame}
	\begin{block}{WP 5.2 - Status}
		\begin{itemize}
			\item Evaluation of passive methods to mitigate discharge fluctuations in Nidelva and Ume \"alv. \pause \tikz\draw[black,fill=yellow] (0,0) circle (.5ex);\pause
			\item Validate flow models of Nidelva and Ume a\"lv under present regime. \pause \tikz\draw[black,fill=green] (0,0) circle (.5ex);\pause
			\item Model possible future scenarios (including 30 start and stops). \pause \tikz\draw[black,fill=blue] (0,0) circle (.5ex);\pause
			\item Model future scenarios with passive and active (WP 5.1) mitigations. \pause \tikz\draw[black,fill=blue] (0,0) circle (.5ex);
		\end{itemize}
	\end{block}
\end{frame}

\begin{frame}
	\frametitle{Miscellaneous - Courses}
	\begin{columns}
		\begin{column}[t]{0.33\textwidth}
			\begin{block}{Completed courses}
				\begin{itemize}
					\item Academic Publishing: Autumn 2018 (4.5 credits)
					\item Viscous Flow - Spring 2019: (12 credits)
				\end{itemize}
			\end{block}
		\end{column}
		\begin{column}[t]{0.33\textwidth}
	\begin{block}{Ongoing courses}
		\begin{itemize}
			\item Computational Fluid Dynamics: Autumn 2019 - Spring 2020 (12 credits)
		\end{itemize}
	\end{block}
\end{column}
		\begin{column}[t]{0.33\textwidth}
	\begin{block}{Future courses}
		\begin{itemize}
			\item Environmental Fluid Dynamics: In the future (credits to be decided)
			\item Ecology for non-ecologists: Autumn 2020 at SLU in Ume\aa (5 credits)
		\end{itemize}
	\end{block}
\end{column}
	\end{columns}
\end{frame}

\begin{frame}
	\frametitle{Miscellaneous - Teaching}
	\begin{block}{Teaching}
		\begin{itemize}
			\item Physics 1 - F0004t
			\begin{itemize}
				\item Several lectures in experimental methods
				\item Labs in experimental methods
				\item Problem sessions in mechanics				
			\end{itemize}
			\item Dam 2: Dams and Dam Safety - G7004B
			\begin{itemize}
				\item One lecture on hydrostatics
				\item Two lectures on open-channel flow
			\end{itemize}
		\end{itemize}
	\end{block}
\end{frame}

\begin{frame}
	\frametitle{Miscellaneous - Consultant Work for Vattenfall}
	\begin{block}{Field work autumn 2019}
		\begin{itemize}
			\item Bathymetry measurements in Jokkmokk using an autonomous USV.
		\end{itemize}
	\end{block}
	\begin{columns}
		\begin{column}{0.5\textwidth}
			\begin{figure}
				\includegraphics[width=1\linewidth]{figures/otter.jpg}
				\caption{USV in field in Jokkmokk (my picture autumn 2019).}
			\end{figure}

		\end{column}
		\begin{column}{0.5\textwidth}
			\begin{figure}
				\includegraphics[width=1\linewidth]{figures/bathymetry.jpg}
				\caption{Example of measured bathymetry with USV.}	
			\end{figure}
			\end{column}
	\end{columns}
\end{frame}


\begin{thebibliography}{9}
	\bibitem{augier2019shallow}
	\textit{Shallow water wave turbulence},
	Augier, Pierre and Mohanan, Ashwin Vishnu and Lindborg, Erik,
	Journal of Fluid Mechanics,
	2019,
	Cambridge University Press.

\end{thebibliography}

\end{document}