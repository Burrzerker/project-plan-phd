# Paper 1 - Case Study of Damping Properties in Stornorrfors Bypass Reach
- Continuation of conference paper presented at Delft.


## Previous work
- Conference paper Panama.
- Conference paper Delft.
- Manning distribution.
- Closing time.

## Work for this paper
- Mesh study
- Richardson extrapolation
- Flood-Routing curves, kolla upp
- Include the results from conference paper
  - Normalized water levels
  - Wetted area
  - Manning distribution vs Uniform Manning
- Couple wetted area with habitats
- Froude

## Structure
- Mesh
  - Mesh study
  - Ricardson
- Dynamics
  - Normalized water levels
  - Wetted area
  - Manning vs Uniform
  - (Froude)
  - (Non normalized water levels)
- Echohydraulics
  - Non normalized water levels
  - Velocities
  - Habitats
  - Gradients
  - Shields

## New Plots
- Extract function that describes the new steady state in all cases and plot in the same plot.
- Compare different points in the transient area, when does 20, 50, 80% of old steady state occur.

## Simulations to do
- Try with different initial flow, i.e 75 and 150
- If Manning distribution should be included then a new simulation with better initial conditions should be done

## Mesh Study
- Richardson Extrapolation
- Water level in validation points for different meshes (local behavior)
- Wetted area for the entire reach for the different meshes (global behavior)

## References that needs to be found
- Reference(s) regarding slowing down the turbine operations, how slowly is it possible to reduce the operation of the turbines.
- References about Delft3D
  - See conference papers
- References about Hydraulics
  - See conference papers
- References about previous hydraulic work in Stornorrfors
  - See conference papers for references to  Gunnar and Anders

- References about previous work in Ume river (if relevant)
- References regarding fish
  - Papers regarding the speed of water level decrease
    - *Factors influencing stranding of wild juvenile brown trout (salmo trutta) during rapid and frequent flow decreases in an artificial stream"*
    - *Field experiments on stranding in juveline atlantic salmon (salmo salar) and brown trout (salmo trutta) during rapid flow decreases caused by hydropeaking*
