# Notes Paper 2

Possible Impact on Salmonoid Habitats in a Bypass Reach Given Flexible Operating Conditions

## Salmonoid Specis in Stornorrfors
    - Atlantic Salmon 
    - Brown Trout
    - European Grayling 

## Possible plots

### 50->21 m^3/s plots
    - Dewatered area (DONE, see dewatered_area.py)
    - Dewatering rate
        - Contour plot of different dewatering rates (see Ana's paper)
        - >20 cm/h
        - 13-20 cm/h
        - 5-13 cm/h
        - 0-5 cm/h
    - Spawning habitats (DONE for all species, see spawning_habitats.py)

### Dynamics plots
    - Area of spawning habitats as a function of time
    - Dewatered area as a function of time (look at Ana's paper)

## Papers on Spawning Habitats
    - Atlantic Salmon (Spawning habitat of Atlantic Salmon and brown trout: General criteria and
intragravel factors)
    - Brown Trout (Spawning habitat of Atlantic Salmon and brown trout: General criteria and
intragravel factors)
    - European Grayling (https://onlinelibrary.wiley.com/doi/epdf/10.1002/rrr.3450030121)
