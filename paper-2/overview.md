# Paper 2 - Numerical Approach for Investigating Ecohydraulics in Stornorrfors Bypass Reach
- Continuation of paper 1 - "Case Study of Damping Properties in Stornorrfors Bypass Reach"
- Preliminary title - Numerical Approach for Investigating Ecohydraulics in Stornorrfors Bypass Reach

## Previous work
- Conference paper Panama.
- Conference paper Delft.
- Paper 1.

## Content of this paper
- Summary of the relevant results from Paper 1
- Ecohydraulics (see the section below)
  - Dry area
  - gradients
  - habitat
  - stranding zones
  - etc

## Ecohydraulics
- Use Delft3D for modelling
- Investigate which species that occurs or could occur in the dry reach (see document stornorrfors_species.md)
- Investigate ecohydraulical paremeters such as
  * Dry area as a function of time and discharge in several cases
  * Water and dry area gradients as a function of damping parameters, time and discharge
  * $Q_{max} / Q_{min}$
  * Areas that are dewatering slower than 10 cm / h at different conditions (see Factors influencing stranding of wild juvenile brown trout...)
  * Possible spawning sites for trout and salmon (Spawning habitat of atlantic salmon and brown trout)

## Structure
- Abstract
- Introduction
- Method
- Results and Discussion

## Simulations to run
- Same as in paper 1
- possibly more if needed

## References that needs to be found
  - References about previous work in Ume river (if relevant)
  - References regarding fish
    - Papers regarding the speed of water level decrease
      - *Factors influencing stranding of wild juvenile brown trout (salmo trutta) during rapid and frequent flow decreases in an artificial stream"*
      - *Field experiments on stranding in juveline atlantic salmon (salmo salar) and brown trout (salmo trutta) during rapid flow decreases caused by hydropeaking*
    - Papers regarding spawning habitat
      - *Spawning habitat of atlantic salmon and brown trout: general criteria and intragravel factors*
