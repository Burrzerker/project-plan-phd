# Pedagogical CV - Anton Burman
A compilation of the courses that I have been involved in during my PHD studies at LTU.

## 2018

- G7004B - Dam II - Dam and Dam Safety
  * One lecture on hydrostatics
  * Two lectures on open-channel flow

## 2019
- F0004t - Physics 1
  * Two lectures on experimental methods
  * Four lab occasions
  * Eight occasions for mechanics exercises
  * Correcting "duggor" for two classes
  * Correcting lab reports for labs on experimental methods, one class
