<!-- # Abstract for IAHR World Conference in Panama 2019

## Title
* Thermo-peaking?
* Flexible hydro power?
* Hydropeaking
* Ecohydraulics
* Natural Damping
* Quantifying Embedded Damping in Rivers
* Embedded Damping in Ume River
* Natural Damping in Ume River
* Embedded Damping in an Old River Bed
* Natural Damping in an Old River Bed

## Motivation
* Ecological impact of hydro power
* Problem with "flexible" hydro power
* Impact on fauna and flora due to thermo-peaking and hydropeaking
* Broader scope is to create a more sustainable, widely accepted hydro power industry
* Find natural damping of Ume River

## Problem Statement

Possible questions that could be answered within the scope of this paper:
* "Figure out how large the natural damping of the rivers are and how minor changes to the shoreline might change the damping." Possibly a similar study for Nidelva can be done also? *This requires the river bed topography*. What is "natural damping"? From context I imagine it can be how long it takes for a river to become entirely stagnant when there is no inflow of water. If this is the definition then changes to the shoreline / riverbed that causes this time to be maximized are advantageous. It would also then be of interest to see how these changes might affect the shear stress of the riverbed, i.e. Darcy's friction factor since flora and fauna are dependent on this. Another definition could be how long time it takes before the water reaches the lower limit of some acceptable flow parameter. It could be of interest to do this study also for the old river bed.
* Defining "natural damping" within ecohydraulics
  * What parameters are intersting?
    - Flow field / velocity
    - Temperature field / temperature (Temperature can trigger drift behaviour for some species)
    - Maybe turbulence parameters, is turbulence intensity maybe of importance?
    - Height of water surface
    - Look at the parameters Knut wrote about in the email, "torrlagd area", "torrläggningstid", "exponeringstid för torrlagda areal"
  * A case with no flow
  * Cases with varying flow
  * Upper limit with "normal flow" maybe
  * Time until a parameter has been achieved is how it can be quantified
* "Peaking effects" at current operating conditions. Both thermal effects as well as water velocity effects. This would probably be necessary for future work when new ramping rates will be used. This study could be a reference case.
* Thermal effects during "nolltappning". Intuitively it feels like the water temperature should be warmer since there is no inflow of new water, this causes the water surface to be lower and thus a lower water volume is subject to the same amount of solar power. Furthermore, during normal spilling conditions the water is usually coming from an upstream dam and this water is usually warmer.  
* Thermal effects in the "mixing" region where the original river bed meets with tunnelviken.
    * There is geometrical data for this region. Also for the region upstream.
    * There is temperature data available from close to the outlet in tunnelviken
    * Compare how cloud coverage can affect the temperature in the river.
    * Assume the temperature at the outlet in Tunnelviken is the same as the inlet in the old river. (Basically the time it takes for the water to travel from the turbine inlet to tunnelviken is so fast that the water is not cooled.)
    * Maybe several cases can be studied, how is the temperature affected during different flow conditions. What happens when the flow in the fish ladder is changed. What happens during spilling?
## Approach
* CFD - Shallow Water Equations in Delft3D
* CFD - CFX if it's necessary?

## Results

## Conclusions
 -->
<!--
### Motivation
hydro power is one of the biggest sources of electricity in Scandinavia. In Sweden approximately 40% of all electricity is produced by hydro power, in Norway this figure is 96%. In Sweden most of the rivers that are suitable for hydro power are already exploited (around 75% according to the Swedish government). The remaining 25% are mostly protected from hydro power exploitation by the Swedish environmental code. This in turn means that the already built hydro power plants have to manage the increase in demand while still being up to environmental standards. One of the big advantages of hydro power is that production can be started and stopped in a short time, this feature means that hydro power is a great compliment to wind power, since wind power is entirely dependent on wind conditions. In Sweden wind power produced 9% of all electricity in 2016 and is expected to grow rapidly in the coming years. As the electricity production becomes more dependent on wind power it also becomes more dependent on the flexibility of hydro power, i.e. how often a power plant can be stopped and started. This in turn means that problems related to highly regulated hydro power needs to be given extra consideration. Problems that are often mentioned in relation to hydro power are for instance influence on fauna and flora due to hydro peaking, erosion of the river banks and commercial interests such as recreational activities. With increasing hydro power demand it becomes increasingly important to quantify damping properties of rivers to be able to properly accommodate the hydro power industry with the current environmental demands.

### Problem Statement
The aim of this study is to see if it's feasible to define an inherent embedded damping property of rivers using the transient behaviour of different parameters relevant to the flow and ecology of the river. It's also of interest to identify geometrical changes that can be made to the river bed or the river banks to optimize this property. Parameters that will be investigated are water levels, water velocities, river bed area of exposure as well as exposure time.

The river under investigation is a part of the Ume river in northern Sweden, more specifically the old river bed that was left partially dry after the hydro power station Stornorrfors was built. Today this part of the river is used for upstream fish migration as well as spillway outlets. This means that the study area is subject to very different flow conditions varying from no flow in winter time to potentially the full flow of the river. Even if there's no hydro power outlet in this part of the river conditions similar to hydro peaking might occur due to spilling. Conclusions from this can then be generalized to other rivers that are subject to similar conditions.

For this study the open-source CFD solver Delft3D-FLOW will be used. Delft3D-FLOW solves the two dimensional unsteady shallow water equations for the horizontal velocity components. For the vertical component the $\sigma$-formulation is used. -->


## Embedded Damping in Ume River
Keywords: Ecohydraulics, CFD, Delft3D

### Abstract
Hydro power is one of the biggest sources of electricity in Scandinavia. In Sweden approximately 40% of all electricity is produced by hydro power, in Norway this figure is 96%. In Sweden most of the rivers that are suitable for hydro power are already exploited, the remaining rivers are protected from hydro power exploitation by the Swedish environmental code. This in turn means that the hydro power plants that are already in operation have to manage the increase in demand while still being up to environmental standards. As the electricity production becomes more dependent on wind power it also becomes more dependent on the flexibility of hydro power, i.e. how often a power plant can be stopped and started. Problems that are often mentioned in relation to hydro power are for instance influence on fauna and flora due to hydro peaking, erosion of river banks and public interests such as recreational activities. With increasing hydro power demand it becomes increasingly important to quantify damping properties of rivers to be able to properly align the hydro power industry with the current environmental demands. The river under investigation is a part of the Ume river in northern Sweden that was left partially dry after the hydro power station Stornorrfors was built. Today this part of the river is used for upstream fish migration as well as spillway outlets. This means that the study area is subject to very different flow conditions varying from no flow in winter time to potentially the full flow of the river. The aim of this study is to see if it's feasible to define an inherent embedded damping property of rivers using the transient behaviour of different parameters relevant to the fluid dynamics and ecology of the river. It's also of interest to identify geometrical changes that can be made to the river bed or the river banks to optimize this property. For this study the open-source CFD solver Delft3D-FLOW will be used. Delft3D-FLOW solves the two-dimensional unsteady shallow water equations for the horizontal velocity components. Delft3D-FLOW also allows three-dimensional solutions by using the $\sigma$-formulation to solve for the vertical component. In this study a comparison between these two formulations will be presented. Parameters that will be investigated are water levels, water velocities, river bed area of exposure as well as exposure time.
