# Page 1

* Introduce myself, I am Anton Burman a PHD student at the division of fluid and experimental mechanics, my supervisors and co-authors are Anders Andersson and Gunnar Hellström.


# Page 2
* In Sweden hydro power produces approximately 40% of the power, in Norway it’s more than to 95%.
* Some popular intermittent sources are for instance solar panels, wind turbines and wave power plants
* Intermittent power sources are in their nature dependent on weather conditions. Wind turbines can only produce power when it’s windy and solar panels are much less efficient when it’s cloudy. Wave power plants can only produce power when it’s windy etc.
Hydro power can be used as a buffer in times of low intermittent power production. Since intermittent power is dependent on weather conditions so is hydro power. In times of favorable weather hydro power production can be reduced in order to store water. This water can then be used later.
Most hydro power plants have some sort of flow conditions that they need to fulfill. Often it is a minimum flow.
Stornorrfors is the power plant that produces the most electricity in all of Sweden. It is close to the outlet of the Ume river, close upstream there is a tributary called Vindel river that. Both the Ume and Vindel river are significant in size.
The vindel river is unexploited and is protected by Swedish law, the Ume river is however fully exploited. The Stornorrfors plant is a significant obstacle for the migrating salmon and trout.
