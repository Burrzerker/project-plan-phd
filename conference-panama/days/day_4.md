# Thursday 5th September
Wednesday was field trip day.

## Session 1: Theme C

### Presentation 1
- presenter didn't arrive

### Presentation 2
- presenter didn't arrive
- interesting topic, fuzzy method for river health

### Presentation 3
- presenter didn't arrive

### Presentation 4
- presenter didn't arrive

### Presentation 5
- development of a duration based management framework for in stream construction induced suspended sediment
- doing work along the wetted perimeter causes sediments to be released
- fish are actually affected by the sediment concentration in the water
- *not interesting to me*

### Presentation 6
- treating drinking water from upland catchment in a disaster relief situation
- *probably not interesting*

## Session 2: Theme C

### Presentation 1
- The environmental quality analysis of the bank protection revetment in the pirai river located in bolivia 33 years after its construction and comparison with natural river bank
- *not interesting for me*

### Presentation 2
- determinning environmental flows for flushing of fines from rivers
- mapping flushing flow with benthic fauna
- *Possibly interesting for me*

### Presentation 3
- landscape and ecological water requirement of ancient town river, lijang world heritage
- ecological water requirement, r2 cross, montana method, 7q10 method, river function calculation, golden ratio section  method
- *probably interesting to see different ecological water requirements*

### Presentation 4
- application of axial flow pump system for improving urban water environment
- *not interesting to me*

### Presentation 5
- investigating climate and land use change scenarios in the magdalena cauca macrobasin
- dynamic dimension search
- *not relevant for me*

### Presentation 6
- conservation and restoration of wetlands to improve their natural purification function
- *not interesting*

## Session 3: Theme C  

### Presentation 1
- my presentation

### Presentation 2
- characterization of spatial variability of precipitation by spatial moments of catchment rainfall in tropical environment
- *not interesting to me*
