# Publication Strategy

## Publications so far
- HydroFlex internal deliverable report (LTU, NTNU)
- Poster at HydroFlex meeting in Trondheim in May 2019 (LTU)
- Poster at StandUP academy in Stockholm May 2019 (LTU)
- Poster at EERA JP Hydropower in Brussels September 2019 (LTU)
- Conference paper IAHR World Congress Panama September 2019 (Inherent Damping in a Partially Dry River) (LTU)

## Future Publications
- "Investigating Damping Properties in a Bypass River" abstract submitted for River Flow conference in Delft, July 2020 (LTU)
- Journal paper for the work done in the dry reach. Further focus on ecohydraulics could be necessary for publication (LTU, VRD)
- After downstream bathymetry has been measured a paper investigating the 30 start / stop per day scenario will be done. Possibly this can be split into three papers
  - One paper modeling the downstream reach with current operating conditions, taking into account the sea water level, the tributaries and the tail race. For validation 3 or 4 loggers will be planted by the people at SLU, measuring water level (LTU, SLU)
  - One paper investigating the hydraulics of the river when subject to 30 start and stops (LTU)
  - One paper investigating the influence of ecology and "social acceptance" (LTU, NTNU, NINA, SLU)
- ACUR (LTU, NTNU)
  - Implementing ACUR model in previous hydraulic models

## Potential Future Publications
- ACUR
  - Optimizing ACUR
  - Different volumes of ACUR
  - Other technical solutions
  - Temperature
- Sediment
  - Collaboration with Kevin
- Photogrammetry
  - Collaboration with Hang
