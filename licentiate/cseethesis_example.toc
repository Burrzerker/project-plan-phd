\ttl@change@i {\@ne }{chapter}{0.7cm}{\vspace {0.3cm}}{\contentslabel {1.7cm}}{\hspace {-0.7cm}}{\hfill \contentspage }\relax 
\ttl@change@v {chapter}{}{}{}\relax 
\contentsline {part}{Part I}{1}
\contentsline {chapter}{{\sc Chapter} \textnormal {1 --} \sc Thesis Introduction}{3}
\contentsline {section}{\numberline {1.1}General information}{3}
\contentsline {subsection}{\numberline { 1.1.1}About the document class}{3}
\contentsline {subsection}{\numberline { 1.1.2}About this document}{4}
\contentsline {section}{\numberline {1.2}Chapters}{4}
\contentsline {subsection}{\numberline { 1.2.1}Defining chapters}{4}
\contentsline {subsection}{\numberline { 1.2.2}Importing chapter contents}{4}
\contentsline {section}{\numberline {1.3}How to append papers}{4}
\contentsline {section}{\numberline {1.4}Cross-references}{5}
\contentsline {section}{\numberline {1.5}Appendices}{5}
\contentsline {section}{\numberline {1.6}Including bibliography lists}{5}
\contentsline {section}{\numberline {1.7}How to compile your project}{6}
\contentsline {section}{\numberline {1.8}Revision history}{6}
\contentsline {chapter}{{\sc Chapter} \textnormal {2 --} \sc Table of contents entry}{9}
\contentsline {section}{\numberline {2.1}First section of the second chapter}{9}
\contentsline {section}{\numberline {2A}This is an appendix section}{11}
\contentsline {subsection}{\numberline {2A.1}Subsection 1}{11}
\contentsline {subsection}{\numberline {2A.2}Subsection 2}{11}
\contentsline {section}{\numberline {2B}This is another appendix section}{11}
\contentsline {chapter}{{\sc Chapter} \textnormal {3 --} \sc Nonsense chapter}{13}
\contentsline {chapter}{\sc References}{19}
\contentsline {part}{Part II}{21}
\contentsline {chapter}{{\sc Paper} \textnormal {\textnormal {A}} \sc }{23}
\contentsline {section}{\numberline {1}Introduction}{25}
\contentsline {section}{\numberline {A}First appendix of paper A}{25}
\contentsline {subsection}{\numberline {A.1}A subsection of the appendix}{25}
\contentsline {subsection}{\numberline {A.2}Another subsection of the appendix}{25}
\contentsline {subsubsection}{Test subsubsection}{25}
\contentsline {section}{\numberline {B}Another appendix}{25}
\contentsline {chapter}{{\sc Paper} \textnormal {\textnormal {B}} \sc }{27}
\contentsline {section}{\numberline {1}Introduction}{29}
\contentsline {section}{\numberline {A}First appendix of paper B}{29}
\contentsline {subsection}{\numberline {A.1}A subsection of the appendix}{29}
\contentsline {subsection}{\numberline {A.2}Another subsection of the appendix}{29}
\contentsline {section}{\numberline {B}Another appendix}{29}
\contentsline {chapter}{{\sc Paper} \textnormal {\textnormal {C}} \sc }{31}
\contentsline {section}{\numberline {1}Introduction}{33}
\contentsline {chapter}{{\sc Paper} \textnormal {\textnormal {D}} \sc }{35}
\contentsline {section}{\numberline {1}Introduction}{37}
\contentsfinish 
