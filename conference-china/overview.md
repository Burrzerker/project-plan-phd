# Conference paper 2022

## Paper
- Preliminary title "Ecohydraulical applications and limitations of calibrated 2D models" 
### Contents
- Background of numerical modelling and ecohydraulics (quite extensive ideally)
- Examples of calibration yielding different suitable manning numbers
  - Based on measured velocity the Manning number is higher
  - Based on depth measurement the Manning number is lower
  - Discussion around data assimilation as a method of calibration
- Discussion about the problem of resolution
  - Grid resolution in a reach is so large that structures will not be modelled
  - GPS location of i.e. depth measurements are very precise, however, the 2D modell might not have any node in proximity of the measurements.
- Discussion / Examples of situations where 2D modelling is sufficient
- Discussion / Examples of situations where 3D modelling is needed

# Abstract 
In the near future, the operating conditions of hydropower plants in the Nordics is expected to change to be a complement to intermittent power production such as solar- and wind-power. This in turn might increase the number and frequency of short term flow regulations, so called hydropeaking. In recent years, hydropowers' effect on local ecosystems has been given a lot of attention. Especially hydropeaking has been shown to affect the ecosystems and bathymetry in the downstream reach. In order to assess the potential impact of a change in operating conditions, one can use different numerical models to model the downstream reach when subject to potential future operating conditions. In-flow parameters, such as depth and velocity, can be predicted by using numerical approximations of the Navier-Stokes equations in either one-, two- or three-dimensions. These modeling approaches have different advantages and disadvantages. In this work we investigate uncertainties that arise naturally in numerical modelling, and how it might affect in-flow parameters, used specifically in ecohydraulics, with special emphasis on two-dimensional models. The flow in a regulated reach was here modelled with the open-source hydrodynamics solver Delft-3D. A stretch of the Ume River in northern Sweden was modelled based on bathymetry measurements from a side-scan sonar. The model was calibrated with depth measurements and validated with ADCP velocity measurements. The results show that the model is very dependent on the downstream boundary condition. Additonaly, it is shown that the spatial resolution of two-dimenionsal models might be insufficient for some ecohydraulical applications. Lastly, a discussion on the merits of one-, two and three-dimensional models is presented.
